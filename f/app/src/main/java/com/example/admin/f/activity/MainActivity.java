package com.example.admin.f.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.admin.f.R;
import com.example.admin.f.fragments.AboutFragment;
import com.example.admin.f.fragments.ArtistsFragments;
import com.example.admin.f.fragments.BaseFragment;
import com.example.admin.f.fragments.ContactFragment;
import com.example.admin.f.fragments.EventsFragment;
import com.example.admin.f.fragments.MapFragment;
import com.example.admin.f.navigation.MenuView;
import com.example.admin.f.navigation.NavigationInterface;
import com.example.admin.f.navigation.TopBar;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

public class MainActivity extends AppCompatActivity implements NavigationInterface, MenuView.MenuInteractions, TopBar.TopBarInteractions, FragmentManager.OnBackStackChangedListener {

    private TopBar topBar;
    private DrawerLayout drawerLayout;
    private MenuView menuView;
    private FragmentManager manager;

    @Override
    protected void onDestroy() {
        addNotification();
        savePreferences();
        super.onDestroy();
    }

    private void addNotification(){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Wyłączyłeś aplikacje")
                .setContentText("Mamy nadzieje że wkrótce nas ponownie odwiedzisz")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FlowManager.init(new FlowConfig.Builder(this).build());
        //FlowManager.getDatabase(MyDatabase.class).reset(this);
        manager = getSupportFragmentManager();
        savePreferences();
        findViews();
        setListeners();
        changeFragment(new EventsFragment(), false);
    }

    private void savePreferences(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("TYP", false);
        editor.commit();
    }

    private void findViews() {
        topBar = findViewById(R.id.top_bar);
        drawerLayout = findViewById(R.id.drawer_layout);
        menuView = findViewById(R.id.menu);
    }

    private void setListeners() {
        menuView.setMenuInteractions(this);
        topBar.setTopBarInteractions(this);
        manager.addOnBackStackChangedListener(this);
    }

    private void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.END);
    }

    private void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.END);
    }

    @Override
    public void changeFragment(BaseFragment fragment) {
        navigateTo(fragment, true);
    }

    @Override
    public void changeFragment(BaseFragment fragment, boolean addToBackStack) {
        navigateTo(fragment, addToBackStack);
    }

    private void navigateTo(Fragment fragment, boolean addToBackStack) {
        if (manager == null || fragment == null) {
            return;
        }

        if (manager.getFragments() != null) {
            Fragment current = manager.findFragmentById(R.id.fragment_container);
            if (current != null && fragment.getClass().equals(current.getClass())) {
                Log.w("BaseActivity", "Fragment navigation failed, possible duplicate entry %s");
            }
        }

        FragmentTransaction transaction = manager.beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(fragment.toString());
        }
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    @Override
    public void goBack() {
        getSupportFragmentManager().popBackStack();
    }

    // -------------- MENU INTERACTIONS !
    @Override
    public void onEventClick() {
        changeFragment(new EventsFragment());
        closeDrawer();
    }

    @Override
    public void onArtistClick() {
        changeFragment(new ArtistsFragments());
        closeDrawer();
    }

    @Override
    public void onAboutClick() {
        changeFragment(new AboutFragment());
        closeDrawer();
    }

    @Override
    public void onMapClick() {
        changeFragment(new MapFragment());
        closeDrawer();
    }

    @Override
    public void onContactClick() {
        changeFragment(new ContactFragment());
        closeDrawer();
    }


    // ---------------- TopBar INTERACTIONS
    @Override
    public void onHamburgerClick() {
        openDrawer();
    }

    @Override
    public void onBackArrowClick() {
        onBackPressed();
    }

    // ------------------- BACKSTACK

    @Override
    public void onBackStackChanged() {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            topBar.showBackArrow(true);
        } else {
            topBar.showBackArrow(false);
        }
    }
}

