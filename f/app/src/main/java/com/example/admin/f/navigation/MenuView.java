package com.example.admin.f.navigation;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.admin.f.*;

public class MenuView extends RelativeLayout {

    private LinearLayout event, artist, about, map, contact;
    private MenuInteractions menuInteractions;

    public MenuView(Context context) {
        super(context);
        init(context);
    }

    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public MenuView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.menu_view, this);
        event = findViewById(R.id.events);
        artist = findViewById(R.id.artist);
        about = findViewById(R.id.about);
        map = findViewById(R.id.map);
        contact = findViewById(R.id.contact);


        event.setOnClickListener(view -> {
            if (menuInteractions != null) {
                menuInteractions.onEventClick();
            }
        });

        artist.setOnClickListener(view -> {
            if (menuInteractions != null) {
                menuInteractions.onArtistClick();
            }
        });

        about.setOnClickListener(view -> {
            if (menuInteractions != null) {
                menuInteractions.onAboutClick();
            }
        });

        map.setOnClickListener(view -> {
            if (menuInteractions != null) {
                menuInteractions.onMapClick();
            }
        });

        contact.setOnClickListener(view -> {
            if (menuInteractions != null) {
                menuInteractions.onContactClick();
            }
        });
    }

    public void setMenuInteractions(MenuInteractions menuInteractions) {
        this.menuInteractions = menuInteractions;
    }

    public interface MenuInteractions {

        void onEventClick();
        void onArtistClick();
        void onAboutClick();
        void onMapClick();
        void onContactClick();
    }
}
