package com.example.admin.f.adapters;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.admin.f.models.Events;
import com.example.admin.f.R;
import com.example.admin.f.fragments.EventsInfoFragment;
import com.squareup.picasso.Picasso;

import static com.example.admin.f.fragments.EventsFragment.navigationInterface;
import static com.raizlabs.android.dbflow.config.FlowManager.getContext;


public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private ArrayList<Events> events;

    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.cell_events, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsAdapter.ViewHolder holder, int position) {

        Picasso.with(getContext()).load(Uri.parse(events.get(position).getImg())).into(holder.image);
        holder.title.setText(events.get(position).getTitle());
        holder.date.setText(events.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public void setData(ArrayList<Events> events) {
        this.events = events;
        if (this.events == null) {
            this.events = new ArrayList<>();
        }
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        TextView date;


        @SuppressLint("ResourceAsColor")
        public ViewHolder(final View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (sharedPreferences.getBoolean("TYP", false))
                title.setTextColor(R.color.colorAccent);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventsInfoFragment moreInfo = new EventsInfoFragment();
                    Bundle args = new Bundle();
                    args.putInt("index", getAdapterPosition());
                    moreInfo.setArguments(args);
                    navigationInterface.changeFragment(moreInfo);
                }
            });
        }
    }
}
