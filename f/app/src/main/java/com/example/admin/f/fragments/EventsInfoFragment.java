package com.example.admin.f.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.f.activity.DisplayImageActivity;
import com.example.admin.f.models.Events;
import com.example.admin.f.R;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.example.admin.f.fragments.EventsFragment.navigationInterface;
import static com.raizlabs.android.dbflow.config.FlowManager.getContext;


public class EventsInfoFragment extends BaseFragment{

    ImageView image;
    TextView title;
    TextView date;
    TextView desc;

    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events_plus, container, false);

        image = (ImageView) view.findViewById(R.id.image);
        title = (TextView) view.findViewById(R.id.title);
        date = (TextView) view.findViewById(R.id.date);
        desc = (TextView) view.findViewById(R.id.desc);
        String url = "";

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (sharedPreferences.getBoolean("TYP", false))
            title.setTextColor(R.color.colorAccent);

        Bundle b = getArguments();
        if(b != null){
            Integer index = b.getInt("index");
            ArrayList<Events> eventsList = (ArrayList<Events>) SQLite.select().from(Events.class).queryList();

            if (index <= eventsList.size()) {
                url = eventsList.get(index).getImg();
                Picasso.with(getContext()).load(Uri.parse(url)).into(image);
                title.setText(eventsList.get(index).getTitle());
                date.setText(eventsList.get(index).getDate());
                desc.setText(eventsList.get(index).getDesc());
            }
        }

        String finalUrl = url;
        image.setOnClickListener(view1 -> {
            /*EventsInfoFragment moreInfo = new EventsInfoFragment();
            Bundle args = new Bundle();
            args.putString("img", finalUrl);
            moreInfo.setArguments(args);
            navigationInterface.changeFragment(moreInfo);*/

            Intent intent = new Intent(getContext(), DisplayImageActivity.class);
            intent.putExtra("URL", finalUrl);
            startActivity(intent);
        });

        return view;
    }
}
