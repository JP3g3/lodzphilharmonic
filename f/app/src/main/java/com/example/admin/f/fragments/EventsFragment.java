package com.example.admin.f.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.f.dialogs.PreferredSourceDialog;
import com.example.admin.f.models.Events;
import com.example.admin.f.navigation.NavigationInterface;
import com.example.admin.f.R;
import com.example.admin.f.adapters.EventsAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.squareup.picasso.Picasso;

import java.sql.Blob;
import java.util.ArrayList;


public class EventsFragment extends BaseFragment implements PreferredSourceDialog.DialogInteraction {

    private DatabaseReference mDatabase;
    private EventsAdapter eventsAdapter = new EventsAdapter();
    public static NavigationInterface navigationInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof NavigationInterface) {
            navigationInterface = (NavigationInterface) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);

        RecyclerView newsList = (RecyclerView) view.findViewById(R.id.news_list);
        newsList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        newsList.setAdapter(eventsAdapter);

        eventsAdapter.setData(null);

        if(isNetworkAvailable()){

            loadDataFromDB(new MyCallback() {
                @Override
                public void onCallback(ArrayList<Events> events) {
                    saveOrUpdateInLocalDB(events);
                    eventsAdapter.setData((ArrayList<Events>) SQLite.select().from(Events.class).queryList());
                }

                private void saveOrUpdateInLocalDB(ArrayList<Events> events) {
                    for (Events n: events) {
                        n.save();
                    }
                }
            });
        }
        else {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            Boolean b = sharedPreferences.getBoolean("TYP", false);
            if (b == true) {
                eventsAdapter.setData((ArrayList<Events>) SQLite.select().from(Events.class).queryList());
            }
            else {
                PreferredSourceDialog open = new PreferredSourceDialog();
                open.setInteraction(this);
                open.show(getFragmentManager(), "");
            }
        }

        return view;
    }


    private void loadDataFromDB(MyCallback myCallback) {
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("events").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Events> events = new ArrayList<>();
                int id = 1;
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    Events n = noteDataSnapshot.getValue(Events.class);
                    events.add(new Events(id, n.getTitle(), n.getDate(), n.getDesc(), n.getImg()));
                    id++;
                }
                myCallback.onCallback(events);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Sprawdź połączenie z internetem", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onDismiss() {
        eventsAdapter.setData((ArrayList<Events>) SQLite.select().from(Events.class).queryList());
    }

    public interface MyCallback {
        void onCallback(ArrayList<Events> events);
    }

}
