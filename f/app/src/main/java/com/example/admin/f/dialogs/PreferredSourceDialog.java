package com.example.admin.f.dialogs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.admin.f.R;

public class PreferredSourceDialog extends DialogFragment {

    private DialogInteraction interaction;

    public void setInteraction(DialogInteraction interaction) {
        this.interaction = interaction;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_open, container, false);
        Button open = view.findViewById(R.id.open);
        Button close = view.findViewById(R.id.close);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();

        open.setOnClickListener(view1 -> {
            interaction.onDismiss();
            editor.putBoolean("TYP", true);
            editor.commit();
            dismiss();
        });

        close.setOnClickListener(view2 -> {
            dismiss();
        });

        return view;
    }

    public interface DialogInteraction {
        void onDismiss();
    }
}
