package com.example.admin.f.models;

import com.example.admin.f.db.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

@Table(database = MyDatabase.class)
public class Events extends BaseModel {
    @PrimaryKey
    @Column
    int id;

    @Column
    String title;

    @Column
    String date;

    @Column
    String desc;

    @Column
    String img;

    public Events() {}

    public Events(int id, String title, String date, String desc, String img) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.desc = desc;
        this.img = img;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getDesc() {
        return desc;
    }

    public String getImg() {
        return img;
    }

    public int getId() {
        return id;
    }
}
