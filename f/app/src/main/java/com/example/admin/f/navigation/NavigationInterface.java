package com.example.admin.f.navigation;

import android.app.FragmentManager;

import com.example.admin.f.fragments.*;

public interface NavigationInterface {

    void changeFragment(BaseFragment fragment);
    void changeFragment(BaseFragment fragment, boolean addToBackStack);
    void goBack();
    FragmentManager getFragmentManager();

}
