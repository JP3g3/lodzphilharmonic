package com.example.admin.f.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.example.admin.f.R;
import com.squareup.picasso.Picasso;

public class DisplayImageActivity extends AppCompatActivity {

    private ImageView image;
    private ImageView cross;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_image_activity);

        image = (ImageView) findViewById(R.id.img);
        cross = (ImageView) findViewById(R.id.cross);

        String url = getIntent().getStringExtra("URL");
        if (url != null){
            Picasso.with(getApplicationContext()).load(Uri.parse(url)).into(image);
        }

        cross.setOnClickListener(view1 -> {
            finish();
        });

    }
}
