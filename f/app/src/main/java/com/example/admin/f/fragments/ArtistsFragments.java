package com.example.admin.f.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.f.R;


public class ArtistsFragments extends BaseFragment implements View.OnClickListener{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artists, container, false);

        setListener(R.id.img1, view);
        setListener(R.id.img2, view);
        setListener(R.id.img3, view);

        return view;
    }

    private void setListener(int resId, View view) {
        View subview = view.findViewById(resId);
        if (subview != null) {
            subview.setOnClickListener(this);
        }
    }

    private void openFragments(int index){
        ArtistsInfoFragments more = new ArtistsInfoFragments();
        Bundle args = new Bundle();
        args.putInt("index", index);
        more.setArguments(args);
       // toolbarTitle.push("Artyści FŁ");
        getNavigation().changeFragment(more);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img1:
                openFragments(1);
                break;
            case R.id.img2:
                openFragments(2);
                break;
            case R.id.img3:
                openFragments(3);
                break;
            default:
                break;
        }
    }
}
