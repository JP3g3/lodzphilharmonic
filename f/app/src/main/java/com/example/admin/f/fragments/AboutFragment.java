package com.example.admin.f.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.f.R;
import com.example.admin.f.adapters.ViewPagerAdaper;

import java.util.ArrayList;
import java.util.List;


public class AboutFragment extends BaseFragment{

    private ViewPager viewPager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        viewPager = view.findViewById(R.id.view_pager);
        ViewPagerAdaper adaper = new ViewPagerAdaper();

        viewPager.setAdapter(adaper);
        List<Integer> photos = new ArrayList<>();
        photos.add(R.drawable.fl2);
        photos.add(R.drawable.kolory_polski3);
        photos.add(R.drawable.fl3);
        adaper.setPhotos(photos);
        return view;
    }
}
